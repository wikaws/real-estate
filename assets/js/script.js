// Handles loading the events for <model-viewer>'s slotted progress bar
const onProgress = (event) => {
  const progressBar = event.target.querySelector('.progress-bar');
  const updatingBar = event.target.querySelector('.update-bar');
  updatingBar.style.width = `${event.detail.totalProgress * 100}%`;
  if (event.detail.totalProgress === 1) {
    progressBar.classList.add('hide');
    event.target.removeEventListener('progress', onProgress);
  } else {
    progressBar.classList.remove('hide');
  }
};
const modelViewer = document.querySelector('model-viewer');
var descElement = document.querySelector(".c-wrapper").offsetHeight;
var btnScanElement = document.querySelector(".c-button-wrapper").offsetHeight;
var urlParams = new URLSearchParams(window.location.search);
var object3D = urlParams.get('object') == null ? null : urlParams.get('object');

document.querySelector('model-viewer').addEventListener('progress', onProgress);
document.getElementById("ar-button").style.bottom = descElement - (btnScanElement - 18) + "px";

document.querySelector("details").addEventListener("toggle", function() {
 document.getElementById("ar-button").style.bottom = document.querySelector(".c-wrapper").offsetHeight - (document.querySelector(".c-button-wrapper").offsetHeight - 18) + "px";
})

console.log(object3D)

if(object3D != null){
  modelViewer.setAttribute('src','assets/models/'+object3D+'.glb')
  if(object3D != 'kitchen' || object3D != 'livingroom'){
    modelViewer.setAttribute('ios-src','assets/models/'+object3D+'.usdz')
  }
}else{
  modelViewer.setAttribute('src','assets/models/bighouse.glb')
  modelViewer.setAttribute('ios-src','assets/models/bighouse.usdz')
}

modelViewer.addEventListener('load', () => {
  if(object3D != null){
    if (modelViewer.canActivateAR) {
      modelViewer.activateAR();
    } else {
      console.error('AR mode is not supported.');
    }
  }
});